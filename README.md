# Module 06 Case Study - Instacart (in progress)

## Domain: E-commerce

Instacart is a grocery ordering and delivery app that aims to make it easy to fill your refrigerator and pantry with your personal favorites and staples when you need them. Instacart’s data science team plays a big part in providing this delightful shopping experience. Currently,they use transactional data to develop models that predict which products a user will buy again, try for the first time, or add to their cart next during a session.

The dataset is a relational set of files describing customers' orders over time. The dataset is anonymized and contains a sample of over 3 million grocery orders from more than 200,000 Instacart users.

__Tasks:__
As a Big Data consultant, you are helping the datascience team to explore the dataset using Spark:
1. Load data into Spark DataFrame
2. Merge all the data frames based on the common key and create a single DataFrame 
3. Check missing data
4. List the most ordered products (top 10)
5. Do people usually reorder the same previous ordered products?
6. List most reordered products
7. Most important department and aisle (by number of products)
8. Get the Top 10 departments
9. List top 10 products ordered in the morning (6 AM to 11 AM)
10. Create a spark-submit application for the same and print the findings inthelog

__Dataset:__ You can download the required dataset from your LMS.